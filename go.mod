module gitlab.com/rKIR/go_lightgbm

go 1.21.0

require (
	github.com/alden-scientific/golightly v0.0.0-20230831224621-1a6feb0fdd8f
	github.com/ebitengine/purego v0.4.0
)

require golang.org/x/sys v0.7.0 // indirect
